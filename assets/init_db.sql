SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

-- Création table "EMPLOYES"

DROP TABLE IF EXISTS `employes`;
CREATE TABLE `employes` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `nom` varchar(25) CHARACTER SET latin1 NOT NULL,
 `prenom` varchar(25) CHARACTER SET latin1 NOT NULL,
 `rang` int(11) NOT NULL,
 `chef` int(11) NOT NULL,
 `competences` int(11) NOT NULL,
 `ambition` int(11) NOT NULL,
 `salaire` int(11) NOT NULL,
 `sante` int(11) NOT NULL,
 `moral` int(11) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=199 DEFAULT CHARSET=utf8;

-- Création table "USER"

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `nom` varchar(25) CHARACTER SET latin1 NOT NULL,
 `prenom` varchar(25) CHARACTER SET latin1 NOT NULL,
 `empid` int(11) NOT NULL,
 `tours` int(11) NOT NULL,
 `IA` int(11) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8


-- Création table "EVENEMENTS"

DROP TABLE IF EXISTS `evenements`;
CREATE TABLE `evenements` (
 `evenement` varchar(150) CHARACTER SET latin1 NOT NULL,
 `consequence` varchar(200) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- Création table "DEPARTS"

DROP TABLE IF EXISTS `departs`;
CREATE TABLE `departs` (
 `titre` varchar(100) CHARACTER SET latin1 NOT NULL,
 `description` varchar(250) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- Création table "CHOIX"

DROP TABLE IF EXISTS `choix`;
CREATE TABLE `choix` (
 `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8