<?php
/**
 * \file      MDeparts.php
 * \author    François
 * \version   1.0
 * \date      14 Décembre 2017
 * \brief     Gère la table departs
 *
 * \details   Cette classe se charge de faire les requêtes vers la base departs
 *            dans le but de mettre à jour ou de récupérer les données concernant
 *            cette dernière. La table departs sert à garder en mémoire les différents
 *            changements au niveau hierarchique du dernier tour
 *
 */
class MDeparts extends CI_Model {

  /**
   * \brief         fonction permettant d'ajouter une entrée dans la table depart
   * \details
   * \param   $data tableau avec les différentes valeurs de colonnes
   * \return
   */
  public function ajouter($data)
  {
    $this->db->insert('departs', $data);
  }


    /**
     * \brief   fonction permettant de vider la table departs
     * \details permet de vider la table départ à chaque nouveau tour pour n'avoir
     *          que les employés partis au dernier tour
     */
  public function vider()
  {
    $this->db->empty_table('departs');
  }


    /**
     * \brief   fonction permettant de récupérer toutes les entrées de la table departs
     * \details
     * \param
     * \return  tableau avec les différentes entrées de la table departs
     */
  public function get_all()
  {
    $query = $this->db->get('departs');
    return  $query->result_array();;
    }
}
