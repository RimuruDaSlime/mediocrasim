<?php

  /**
   * \file      MEvenements.php
   * \author    François
   * \version   1.0
   * \date      14 Décembre 2017
   * \brief     Gère la table evenements
   *
   * \details    Cette classe se charge de faire les requêtes vers la base evenements
   *             dans le but de mettre à jour ou de récupérer les données concernant
   *             cette dernière. La table choix sert à garder en mémoire les différents évènements
   *             aléatoires qui surviennent d'un tour à l'autre
   *
   */
class MEvenements extends CI_Model {


    /**
     * \brief         fonction pour ajouter un évènement à la base de données
     * \details       L'évènement sera ajouté à la table evenements, qui sera consultée
     *                lorsque l'on souhaite récupérer les évènements aléatoires qui ont eu lieu
     *                au tour précédent
     * \param   $data tableau de données relatives à l'utilisateur
     * \return
     */
  public function ajouter($data)
  {
    $this->db->insert('evenements', $data);
  }

  /**
   * \brief   fonction permettant de vider la table evenements
   * \details
   * \param
   * \return
   */
  public function vider()
  {
    $this->db->empty_table('evenements');
  }

  /**
   * \brief   fonction permettant de récupérer toutes les entrées de la table evenements
   * \details
   * \param
   * \return  tableau avec les différentes entrées de la table evenements
   */
  public function get_all()
  {
    $query = $this->db->get('evenements');
    return  $query->result_array();
  }
}
