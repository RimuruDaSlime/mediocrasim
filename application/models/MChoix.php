<?php
/**
 * \file      MChoix.php
 * \author    François
 * \version   1.0
 * \date      14 Décembre 2017
 * \brief     Gère la table Choix
 *
 * \details    Cette classe se charge de faire les requêtes vers la base choix
 *             dans le but de mettre à jour ou de récupérer les données concernant
 *             cette dernière. La table choix sert à garder en mémoire l'employé que
 *             le joueur souhaite promouvoir
 *
 */
class MChoix extends CI_Model {

  /**
   * \brief         fonction permettant d'ajouter une entrée dans la table choix
   * \details
   * \param   $data tableau avec les différentes valeurs de colonnes
   * \return
   */
  public function create($data)
  {
    $this->db->insert('choix', $data);
  }

  /**
   * \brief   fonction permettant de récupérer toutes les entrées de la table choix
   * \details
   * \param
   * \return  tableau avec les différentes entrées de la table choix
   */
  public function get_all()
  {
    $query = $this->db->get('choix');
    return  $query->result_array();
  }

  /**
   * \brief   supprime une entrée dans la table choix
   * \details
   * \param   $id identifiant de l'entrée à supprimer
   * \return
   */
  public function delete($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('choix');
  }

  /**
   * \brief   fonction permettant de savoir si la table Choix est vide
   * \details
   * \param
   * \return  valeur de vérité de l'assertion "la table choix est vide"
   */
  public function est_vide()
  {
    $query = $this->db->get('choix');
    return $query->num_rows() == 0;
  }

  /**
   * \brief   fonction permettant de vider la table choix
   * \details permet de faire en sorte que l'utilisateur puisse
   *          choisir à chaque tour l'employé qu'il souhaite promouvoir
   * \param
   * \return
   */
  public function vider()
  {
    $this->db->empty_table('choix');
  }


}
