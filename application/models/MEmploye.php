<?php

/**
 * \file      MEmploye.php
 * \author    François
 * \version   1.0
 * \date      18 Novembre 2017
 * \brief     Gère la table employes
 *
 * \details   Cette classe se charge de faire les requêtes vers la base employes
 *            dans le but de mettre à jour ou de récupérer les données concernant
 *            les employés de l'entreprise.
 */

class MEmploye extends CI_Model {

  /**
   * \brief      Ajoute un employé dans la table employes
   * \details    L'employé ajouté prendra les caractéristiques indiquées dans le
   *             tableau passé en argument.
   *
   * \param    data           tableau contenant toutes les caractéristiques de l'employé
   * \return   Un objet \e employe prenant les caractéristiques de la ligne ajoutée.
   */
  public function create($data)
  {
    $this->db->insert('employes', $data);
    return $this->get_by_id($this->db->insert_id());
  }

  /**
   * \brief      Retourne tous les employés de l'entreprise
   * \details    Tous les employés de la table employes seront retournés dans un tableau d'employes.
   *
   * \return    Un tableau d' \e employe dans lequel sont stockés les employées de l'entreprise.
   */
  public function get_all()
  {
    $this->db->order_by('rang');
    $query = $this->db->get('employes');
    $res = $query->result_array();
    $employes = array();

    foreach ($res as $emp) {
      $employes[] = new Employe($emp);
    }

    return $employes;
  }


  /**
   * \brief      Retourne l'employé spécifié par son id
   * \details    L'identifiant spécifié est un numéro unique à chaque employé
   *
   * \param    id             identifiant de l'employé
   * \param    point2         Point 2 pour le calcul de distance.
   * \return    Un objet \e employe contenant l'employé retourné
   */
  public function get_by_id($id)
  {
    $query = $this->db->get_where('employes', array('id' => $id));
    $data = $query->result_array();
    $emp = new Employe($data[0]);
    return $emp;
  }

  /**
   * \brief      Retourne l'id du joueur
   * \details    Le joeur joue un employé dans l'entreprise dont l'id sera renvoyé
   *
   * \return    Un \e int représentant l'identifiant de l'employé joué par le joueur.
   */
  public function get_user_empid()
  {
    $this->db->select('empid');
    $query = $this->db->get('user');
    $res = $query->result_array();
    if(empty($res))
    {
      return -1;
    }
    else {
      return $res[0]['empid'];
    }
  }

  /**
   * \brief      Met à jour un employé dans la table employes
   * \details    Les nouvelles valeurs de la ligne modifiée seront les attribus de
   *             l'objet employé passé en paramètre.
   *
   * \param    emp         employé à modifier dans la base
   */
  public function save($emp)
  {
    $data = array('competences' => $emp->competences(),
                  'ambition' => $emp->ambition(),
                  'moral' => $emp->moral(),
                  'salaire' => $emp->salaire(),
                  'sante' => $emp->sante(),
                  'chef' => $emp->chef(),
                  'nom' => $emp->nom(),
                  'rang' => $emp->rang(),
                  'prenom' => $emp->prenom());
    $this->db->where('id', $emp->id());
    $this->db->update('employes', $data);
  }

  /**
   * \brief      Supprimer un employe dans la table employes
   * \details
   * \param   $id   L'identifiant de l'employe qu'on veut supprimer
   */
  public function renvoyer($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('employes');
  }

  /**
   * \brief   Met à jour un chef dans la table employes
   * \details
   * \param   $ancien   L'identifiant de l'ancien chef
   * \param   $nouveau  L'identifiant du nouveau chef
   * \param   $rang     Un entier representan le rang dans lequel le chef appartient
   */
  public function update_chef($ancien, $nouveau, $rang)
  {
    $this->db->where('chef', $ancien);
    $this->db->where('rang', $rang);
    $this->db->set('chef', $nouveau);
    $this->db->update('employes');
  }

  /**
   * \brief   Recupere l'equipe d'un chef
   * \details
   * \param   $chef   L'identifiant du chef
   * \return  Renvoie une array contenant les employes appartenant a l'equipe du chef
   */
  public function get_equipe($chef)
  {
    $query = $this->db->get_where('employes', array('chef' => $chef));
    $res = $query->result_array();
    $employes = array();
    foreach ($res as $emp) {
      $employes[] = new Employe($emp);
    }
    return $employes;
  }

  /**
   * \brief   Recupere tous les employes du meme rang
   * \details
   * \param   $rang   Un entier representant le rang qu'on veut
   * \return  Renvoie une array contenant les employes du meme rang
   */
  public function get_by_rang($rang)
  {
    $query = $this->db->get_where('employes', array('rang' => $rang));
    $res = $query->result_array();
    $employes = array();
    foreach ($res as $emp) {
      $employes[] = new Employe($emp);
    }
    return $employes;
  }

  /**
   * \brief   Genere la hierarchie des employes
   * \details Genere l'arbre contenant tous les employes genere aleatoirement
   * \param   $hauteur  Hauteur de l'arbre
   * \param   $taille_equipe  Nombre d'employes par equipe
   */
  public function generer_arbre($hauteur, $taille_equipe)
  {
    $rang = 1;
    $boss = $this->emp_aleatoire(0, $rang, $hauteur*1000);
    $employes = array($boss);
    $equipe = array();
    for ($i=0; $i < $hauteur; $i++) {
      $rang = $rang + 1;
      foreach ($employes as $e) {
        for ($j=0; $j < $taille_equipe ; $j++) {
          $emp = $this->emp_aleatoire($e->id(), $rang, (($hauteur+2) - $rang)*1000);
          $equipe[] = $emp;
        }
      }
      $employes = $equipe;
      $equipe = array();
    }
  }

  /**
   * \brief   Recupere le hauteur de l'arbre
   * \details
   * \param
   * \return  Retourne un entier representant le hauteur de l'arbre
   */
  public function get_hauteur()
  {
    $this->db->select_max('rang');
    $res = $this->db->get('employes');
    $res = $res->row();
    return $res->rang;
  }

  /**
   * \brief   Genere un employe aleatoirement
   * \details
   * \param   $chef   Un entier representant l'identifiant du chef de l'employe
   * \param   $rang   Un entier representant le rang dans lequel l'employe appartient
   * \param   $salaire  Un entier representant salaire de l'employe
   * \return  Renvoie un objet Employe aleatoirement genere
   */
  public function emp_aleatoire($chef, $rang, $salaire)
  {
    $nom = $this->randomName();
    $data = array('chef' => $chef,
                  'competences' => rand(30,100),
                  'ambition' => rand(30,100),
                  'moral' => rand(30,100),
                  'salaire' => $salaire,
                  'sante' => rand(60,100),
                  'rang' => $rang,
                  'nom' => $nom['surname'],
                  'prenom' => $nom['forename']);
    $emp = $this->create($data);
    return $emp;
  }

  /**
   * \brief   Fonction qui ameliore les attributs d'un employe
   * \details
   * \param   $id   Un entier representant l'identifiant de l'employe qu'on veut ameliorer
   * \param   $array  Une array contenant les amelioration qu'on veut affectuer
   */
  public function ameliorer($id,$array){
    $query = $this->db->get_where('employes', array('id' => $id));
    $res = $query->result_array();
    $emp = new Employe($res[0]);

    $data = array(
        'competences' => $array[0] + $emp->competences(),
        'ambition' => $array[1] + $emp->ambition(),
        'salaire' => $array[2] + $emp->salaire());

    $this->db->where('id', $id);
    $this->db->update('employes', $data);
  }

  public function modifierMoralTousLesEmployes($modif)
  {
    $m = 'moral + '.$modif;
    $this->db->set('moral', $m, FALSE);
    $this->db->update('employes');
  }

  public function modifierSanteTousLesEmployes($modif)
  {
    $m = 'sante + '.$modif;
    $this->db->set('sante', $m, FALSE);
    $this->db->update('employes');
  }

  public function modifierSalaireTousLesEmployes($modif)
  {
    $m = 'salaire + (salaire *'.$modif.')';
    $this->db->set('salaire', $m, FALSE);
    $this->db->update('employes');
  }

  public function modifierAmbitionTousLesEmployes($modif)
  {
    $m = 'ambition + '.$modif;
    $this->db->set('ambition', $m, FALSE);
    $this->db->update('employes');
  }

  public function modifierCompetencesTousLesEmployes($modif)
  {
    $m = 'competences + '.$modif;
    $this->db->set('competences', $m, FALSE);
    $this->db->update('employes');
  }


  /**
   * \brief   Genere un nom aleatoirement
   * \details
   * \param
   * \return  Renvoie une array contenant le prenom et nom d'un employe
   */
  public function randomName() {
      $forenames = array(
      'Alex','Andie','Ari','Autumn','August',
      'Bailey','Billie','Blaine','Blake',
      'Cameron','Carter','Casey','Chandler','Charlie','Christian',
      'Dallas','Dale','Devin',
      'Elliott','Emerson',
      'Finn','Frankie','Frances','Fayne',
      'Hampton','Haskell',
      'Jordan','Jules','Julian',
      'Kennedy','Kelly','Kaden',
      'Lake','Logan',
      'Max','Morgan',
      'Pat','Peyton',
      'Ray','Reed','Riley','Roan','Rudy','Ryan',
      'Sage','Sam','Shawn','Sean','Stevie','Summer',
      'Tanner','Taylor','Toby','Tyler',
      'Val',
      'West','Winter'
  );

      $surnames = array(
      'Adams','Anderson','Allen','Ali','Armstrong','Andrews','Atkinson','Ahmed',
      'Brown','Bailey','Bell','Baker','Bennett','Butler','Berry','Booth','Brooks',
      'Cable','Cadden','Caddick','Caddy','Cadge','Cadman','Cadogan','Cadwallader',
      'Dacey','Dack','Dacombe','Dagley','Daglish','Dagnell','Daines','Dakin',
      'Eade','Eadie','Eagle','Eales','Ealham','Ealy','Eames','Eansworth',
      'Fabb','Faber','Fackrell','Fadden','Fagan','Fagon','Faherty','Fahy','Fairbair',
      'Gadd','Gadsden','Gaffin','Gaffney','Gaffy','Gailey','Gainsford','Galavan',
      'Hacault','Hack','Hackel','Hackett','Hackney','Hadaway','Hadden','Haddock',
      'Jackman','Jacks','Jackson','Jacob','Jacobs','Jacobson','Jacques','Jaffray','Jagger',
      'Kacy','Kaddour','Kalton','Kamara','Kampfner','Kanane','Kanber','Kane',
      'Labbe','Lace','Lacey','Lack','Lacy','Ladyer','Lafferty','Lagan'
  );
  //Generate a random forename.
  $random_forename = $forenames[mt_rand(0, sizeof($forenames) - 1)];
  //Generate a random surname.
  $random_surname = $surnames[mt_rand(0, sizeof($surnames) - 1)];
  //Concatenation of both names
  $random_fullname = array('forename' => $random_forename, 'surname' => $random_surname);
  //Returns the full name created randomly
  return $random_fullname;
  }

}
