<?php
/**
 * \file      MUser.php
 * \author    François
 * \version   1.0
 * \date      14 Décembre 2017
 * \brief     Gère la table user
 *
 * \details    Cette classe se charge de faire les requêtes vers la base user
 *             dans le but de mettre à jour ou de récupérer les données concernant
 *             cette dernière. La table user sert à garder en mémoire les différents
 *             paramètres de la partie créée par le joeuru
 *
 */
class MUser extends CI_Model {

  /**
   * \brief         fonction pour ajouter l'utilisateur à la base de données
   * \details       L'utilisateur sera ajouté à la table user, qui sera consultée
   *                dès qu'un renseignement sur l'utilisateur est demandé
   * \param   $data tableau de données relatives à l'utilisateur
   * \return
   */
  public function create($data)
  {
    $this->db->insert('user', $data);
  }

  /**
   * \brief   fonction nous retournant le nombre de tours.
   * \details
   * \return  nombre de tours révolus
   */
  public function tours()
  {
    $this->db->select('tours');
    $query = $this->db->get('user');
    return $query->result_array()[0]['tours'];
  }

  /**
   * \brief   incrémente de 1 le nombre de tours dans la base de données
   * \details fonction appelée à chaque fin de tour afin de pouvor renseigner
   *          le joueur sur le nombre de tours passés
   * \param
   * \return
   */
  public function add_tour()
  {
    $this->db->set('tours', 'tours+1', FALSE);
    $this->db->update('user');
  }

  /**
   * \brief   fonction permettant de récupérer le type d'IA sélectionné par l'utilisateur
   * \details le type d'IA a été désigné lors du paramétrage de la partie, et a été
   *          stocké à ce moment là dans la table user
   * \param
   * \return  le type d'IA qui est solicité
   */
  public function get_IA()
  {
    $this->db->select('IA');
    $query = $this->db->get('user');
    return $query->result_array()[0]['IA'];
  }

}
