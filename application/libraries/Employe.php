<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * \file      Employe.php
 * \author    François
 * \version   1.0
 * \date      14 Décembre 2017
 * \brief     contient la classe employés
 *
 * \details   Cette classe permet de créer des objets employés à partir des
 *            données présentes dans la table employes. C'est aussi grâce à elle
 *            que nous sommes capables de calculer certains attribus, comme
 *            la rentabilité, le taux de présence ou la productivité   
 *
 */
class Employe {

  private $CI;
  private $_id;
  private $_chef;
  private $_competences;
  private $_ambition;
  private $_moral;
  private $_salaire;
  private $_sante;
  private $_nom;
  private $_prenom;
  private $_rang;
  /**
   * \brief  Contructeur de la classe Employe
   * \details
   * \param  $params tableau avec les différentes valeurs des champs de l'employé
   */
  public function __construct($params = array())
  {
    $this->hydrate($params);
    $this->CI =& get_instance();
    $this->CI->load->Model('MEmploye');
  }

  /**
   * \brief           permet de construire un objet employé à partir d'un tableau de données
   * \details
   * \param   $data   tableau avec les différentes valeurs des champs
   */
  public function hydrate(array $data) {
    foreach($data as $key => $value) {
      $method = 'set_'.$key;
      if(method_exists($this, $method)) {
        $this->$method($value);
      }
    }
  }

  /**
   * \brief     Calcul de la rentabilité d'un employé
   * \details
   * \return    Un réel représentant la rentabilité d'un employé
   */
  public function rentabilite()
  {
    $rentabilite = $this->productivite();
    $salaires = $this->salaire();
    $equipe = $this->CI->MEmploye->get_equipe($this->id());
    foreach ($equipe as $e) {
      $rentabilite = $rentabilite + $e->rentabilite();
      $salaires = $salaires + $e->salaire();
    }
    return $rentabilite - $salaires;
  }

  /**
   * \brief     Calcule de la productivité d'un employé
   * \details
   * \return    Un réel représentant la productivité d'un employé
   */
  public function productivite()
  {
    $productivite = $this->competences() * $this->moral();
    return $productivite;
  }

  /**
   * \brief     réalise les opérations nécessaires lorsqu'un employé est muté à un poste supérieur
   * \details
   * \param chef nouveau chef de l'employé
   */
  public function augmenter($chef)
  {
    $this->set_rang($this->rang()-1);
    $this->set_salaire($this->salaire() + 1000);
    $this->set_chef($chef);
  }

  /**
   * \brief     Calcule le taux de présence d'un employé
   * \details   le taux de présence est calculé en fonction de la santé, du moral, ainsi que l'ambition de l'employé
   *            grâce au calcul suivant : (santé + moral + ambition)/3
   * \return    Un réel représentant le taux de présence d'un employé
   */
  public function presence()
  {
    $taux = $this->sante() + $this->moral() + $this->ambition();
    $taux = $taux/3 ;
    return $taux;
  }


  /**
   * \brief     Une fonction pour récupérer l'id dun employé
   * \details
   * \return    Un entier représentant l'id d'un employé
   */
  public function id()
  {
    return $this->_id;
  }

  /**
   * \brief     Une fonction pour fixer l'id d'un employé
   * \details
   * \param     $id   Le nouveau identifiant de l'employé
   */
  public function set_id($id)
  {
    $this->_id = $id;
  }

  /**
   * \brief     Une fonction pour récupérer les compétences dun employé
   * \details
   * \return    Un entier représentant les compétences d'un employé
   */
  public function competences()
  {
    return $this->_competences;
  }

  /**
   * \brief     Une fonction pour fixer les compétences d'un employé
   * \details
   * \param     $comp   Les nouvelles compétences de l'employé sous forme entière
   */
  public function set_competences($comp)
  {
    $this->_competences = $comp;
  }

  /**
   * \brief     Une fonction pour récupérer l'ambition d'un employé
   * \details
   * \return    Un entier représentant l'ambition d'un employé
   */
  public function ambition()
  {
    return $this->_ambition;
  }

  /**
   * \brief     Une fonction pour fixer l'ambition d'un employé
   * \details
   * \param     $amb  Les nouvelles ambitions de l'employé sous forme entière
   */
  public function set_ambition($amb)
  {
    $this->_ambition = $amb;
  }

  /**
   * \brief     Une fonction pour récupérer le moral d'un employé
   * \details
   * \return    Un entier représentant le moral d'un employé
   */
  public function moral()
  {
    return $this->_moral;
  }

  /**
   * \brief     Une fonction pour fixer le moral d'un employé
   * \details
   * \param     $moral Le nouveau moral de l'employé
   */
  public function set_moral($moral)
  {
    $this->_moral = $moral;
  }

  /**
   * \brief     Une fonction pour récupérer la salaire dun employé
   * \details
   * \return    Un entier représentant la salaire d'un employé
   */
  public function salaire()
  {
    return $this->_salaire;
  }

  /**
   * \brief     Une fonction pour fixer la salaire d'un employé
   * \details
   * \param     $sa   La nouvelle salaire de l'employé
   */
  public function set_salaire($sal)
  {
    $this->_salaire = $sal;
  }

  /**
   * \brief     Une fonction pour récupérer la santé d'un employé
   * \details
   * \return    Un entier représentant la santé d'un employé
   */
  public function sante()
  {
    return $this->_sante;
  }

  /**
   * \brief     Une fonction pour fixer la santé d'un employé
   * \details
   * \param     $sante   La nouvelle santé de l'employé sous forme entière
   */
  public function set_sante($sante)
  {
    $this->_sante = $sante;
  }

  /**
   * \brief     Une fonction pour récupérer le chef d'un employé
   * \details
   * \return    Un entier représentant l'id de chef de l'employé
   */
  public function chef()
  {
    return $this->_chef;
  }

  /**
   * \brief     Une fonction pour fixer le chef d'un employé
   * \details
   * \param     $chef   Le nouveau chef de l'employé
   */
  public function set_chef($chef)
  {
    $this->_chef = $chef;
  }

  /**
   * \brief     Une fonction pour récupérer le nom d'un employé
   * \details
   * \return    Un string représentant le nom d'un employé
   */
  public function nom()
  {
    return $this->_nom;
  }

  /**
   * \brief     Une fonction pour fixer le nom d'un employé
   * \details
   * \param     $nom   Le nouveau nom de l'employé
   */
  public function set_nom($nom)
  {
    $this->_nom = $nom;
  }

  /**
   * \brief     Une fonction pour récupérer e prenom du'n employé
   * \details
   * \return    Un string représentant le prenom d'un employé
   */
  public function prenom()
  {
    return $this->_prenom;
  }

  /**
   * \brief     Une fonction pour fixer le prenom d'un employé
   * \details
   * \param     $prenom   Le nouveau prenom de l'employé
   */
  public function set_prenom($prenom)
  {
    $this->_prenom = $prenom;
  }

  /**
   * \brief     Une fonction pour récupérer le rang d'un employé dans l'arbre
   * \details
   * \return    Un entier représentant le rang d'un employé
   */
  public function rang()
  {
    return $this->_rang;
  }

  /**
   * \brief     Une fonction pour fixer le rang d'un employé dans l'arbre
   * \details
   * \param     $rang   Le nouveau rang de l'employé
   */
  public function set_rang($rang)
  {
    $this->_rang = $rang;
  }

}
