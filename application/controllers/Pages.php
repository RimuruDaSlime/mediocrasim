<?php
/**
 * \file      Pages.php
 * \author    François
 * \version   1.0
 * \date      14 Décembre 2017
 * \brief     Gère les différentes pages du site
 *
 * \details   Cette classe permet de renvoyer une page demandée pa le client.
 *            C'est aussi cette dernière qui va se charger de faire les différents
 *            calculs et opérations nécessités par ces dernières. 
 *
 */
class Pages extends CI_Controller {

        /**
         * \brief   fonction permettant de rediriger l'utilisateur sur la page qu'il a demandé
         * \details en fonction de la page demandée, un certain nombre de données seront récupérées
         *          de la base de données, et un certain nombre de calculs seront effectués
         * \param   page page sur laquelle l'utilisateur souhaite se rendre
         * \id      argument éventuel (jamais utilisé, mis par convention)
         * \return
         */
        public function view($page = 'accueil', $id=null)
        {
          //import des différentes librairies
          $this->load->library('Employe');
          $this->load->model('MEmploye');
          $this->load->model('MUser');

          //si la page n'existe pas
          if(!file_exists(APPPATH.'views/pages/'.$page.'.php'))
          {
            //affichage de la page d'erreur 404
            show_404();
          }

          //on regarde si le joueur a déjà créé une partie
          if($this->MEmploye->get_user_empid() == -1)
          {
            //si ce n'est pas le cas, on le renvoie sur la page de création de partie
            $this->setup();
          }
          //si le joueur a déjà configuré la partie et créé un personneage
          else
          {
            //on récupère un certain nombre de données de la base de données
            $data['user'] = $this->MEmploye->get_by_id($this->MEmploye->get_user_empid());
            $data['tour'] = $this->MUser->tours();
            //le titre de la page est simplement la page demandée
            $data['title'] = ucfirst($page);

            //si on souhaite se rendre sur l'onglet équipe
            if($page == 'equipe')
            {
              //on récupère les employés de l'équipe pour pouvoir les donner à la page affichée
              $data['employes'] = $this->MEmploye->get_equipe($this->MEmploye->get_user_empid());
            }

            //si c'est sur la page d'accueuil que le joueur souhaite aller
            if($page == 'accueil')
            {
              //import de bibliothèques supplémentaires
              $this->load->model('MDeparts');
              $this->load->model('MEvenements');
              //on récupère le chef de l'employé, les entrées dans les bases de données départs et évènements
              $data['chef'] = $this->MEmploye->get_by_id($this->MEmploye->get_by_id($this->MEmploye->get_user_empid())->chef());
              $data['departs'] = $this->MDeparts->get_all();
              $data['evenements'] = $this->MEvenements->get_all();
            }

            //on charge la page demandée au client
            $this->load->view('templates/header', $data);
            $this->load->view('pages/'.$page, $data);
            $this->load->view('templates/footer');
          }

        }

        /**
         * \brief   fonction servant à créer la partie et à afficher l'écran de configuration
         * \details cette fonction se charge de configurer entièrement la partie en fonction des données
         *          sélectionnées par l'utilisateur lors de la configuration de la partie
         */
        public function setup()
        {
          //import de la libraire MUser
          $this->load->model('MUser');
          //selon la méthode utilisée pour appeler la fonction
          switch($_SERVER['REQUEST_METHOD'])
          {
            //si l'utilisateur veut juste l'écran de configuration pour créer une nouvelle partie
            case 'GET':
              $this->load->view('pages/setup');
              $this->load->view('templates/footer');
              break;
            //si en revanche il a déjà sélectionné les options qu'il souhaite pour créer la partie
            case 'POST':
              //on regarde que tous les champs du formulaire aient bien été renseignés
              if(isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['hauteur']) && isset($_POST['taille']) && isset($_POST['IA']))
              {
                //on charge d'avantage de librairies
                $this->load->model('MEmploye');
                $this->load->model('MUser');
                $this->load->model('MChoix');
                $this->load->model('MEvenements');
                $this->load->library('Employe');


                //on vide la table des employés
                $this->db->empty_table('employes');
                //on vide les entrées dans la table des évènements aléatoires
                $this->MEvenements->vider();
                $this->MChoix->vider();

                //on génère l'arbre hierarchique en lui renseignant la hauteur et la taille des équipes
                $this->MEmploye->generer_arbre($_POST['hauteur'], $_POST['taille']);
                //on récupère tous les employés ainsi créés
                $emps = $this->MEmploye->get_all();

                //variable servant à savoir si on a déjà trouvé l'employé qu'incarnera le joueur
                $found = False;

                //enfin, on renseigne le type d'IA qui sera utilisée dans la partie
                $IA = $this->input->post('IA');

                switch ($IA) {
                  case 'normale':
                    $IA = 0;
                    break;
                  case 'random' :
                    $IA = 1;
                    break;
                  default:
                    $IA = 1;
                    break;
                }

                //on boucle sur la liste des employés pour trouver celui que le joueur incarnera
                for ($i=0; $i < count($emps) && !$found; $i++) {
                  //on prend le premier à la position hauteur -1 (pour qu'il gère une équipe)
                  if($emps[$i]->rang() == $_POST['hauteur'])
                  {
                    //on modifie l'employé avec toutes les données renseignées par le joueur
                    $emp = $emps[$i];
                    $found = True;
                    $emp->set_nom($_POST['nom']);
                    $emp->set_prenom($_POST['prenom']);
                    $emp->set_competences($_POST['competence']);
                    $emp->set_ambition($_POST['ambition']);
                    $emp->set_moral($_POST['moral']);
                    $data = array('prenom' => $emp->nom(), 'nom' =>$emp->prenom(), 'empid' =>$emp->id(), 'tours' => 1, 'IA' => $IA);
                    $this->MUser->create($data);
                    $this->MEmploye->save($emp);
                  }
                }

                //on redirige le joueur vers la page d'accueil
                redirect('index.php');
              }
              break;
          }
        }

        /**
         * \brief   fonction exécutée lorsque le joueur souhaite arrêter la partie
         * \details cette fonction se charge de supprimer l'entrée correspondant à la
         *          partie dans la table user ainsi que tous les employés présents dans
         *          la table employes
         */
        public function demission()
        {
          $this->db->empty_table('user');
          $this->db->empty_table('departs');
          $this->db->empty_table('employes');
          //on redirige
          redirect('index.php');
        }

        /**
         * \brief   sert à afficher le profil de l'employé d'ID passé en argument
         * \details cette fonciton est appelée lorsque l'on souhaite sur l'Arbre
         *          visionner des informations sur un employé directement avec
         *          un clic droit
         * \param   id identifiant de l'employé dont on souhaite visionner le
         *          profil
         */
        public function profil($id)
        {
          //import des librairies nécessaires
          $this->load->library('Employe');
          $this->load->model('MEmploye');

          //si le joueur a bien le droit de visionner le profil de l'employé
          $emp = $this->MEmploye->get_by_id($id);
          $user=$this->MEmploye->get_user_empid();
          if($emp->rang() > $this->MEmploye->get_by_id($this->MEmploye->get_user_empid())->rang() || $emp->id() == $this->MEmploye->get_user_empid())
          {
            $data['emp'] = $emp;
            $data['user'] = $user;
            $this->load->view('pages/profil', $data);
          }
          //s'il n'a pas le droit de visionner le profil
          else
          {
            //affichage d'un message d'erreur
            echo "Vous n'avez pas le droit de visionner le profil de cet employé<br>";
          }

        }

        /**
         * \brief   fonction permettant d'afficher la page de recruttement d'un nouvel
         *          employé lors du renvoi d'un employé
         * \details la fonciton génère aléatoirement 4 employés parmis lesquels le joueur
         *          devra choisir le remplaçant de l'employé qu'il vient juste de renvoyer
         * \param   id identifiant de l'employé que l'utilisateur veut renvoyer
         */
        public function renvoi($id)
        {
          //import des librairies nécessaires
          $this->load->library('Employe');
          $this->load->model('MEmploye');

          //on regarde si le joueur a bien le droit de renvoyer l'employé
          $renvoi = $this->MEmploye->get_by_id($id);
          if($renvoi->chef() == $this->MEmploye->get_user_empid())
          {
            //création des 4 postulants
            $employes = array();
            for($i = 0; $i < 4; $i++)
            {
              $nom = $this->MEmploye->randomName();
              $data = array('id' => $i,
                            'chef' => $renvoi->chef(),
                            'competences' => rand(30,100),
                            'ambition' => rand(30,100),
                            'moral' => rand(30,100),
                            'salaire' => $renvoi->salaire(),
                            'sante' => rand(60,100),
                            'rang' => $renvoi->rang(),
                            'nom' => $nom['surname'],
                            'prenom' => $nom['forename']);
              $emp = new employe($data);
              $employes[] = $emp;
            }
            //on affiche le panneau d'embauche des postulants
            $donnees = array('employes' => $employes );
            $donnees['renvoi'] = $id;
            $this->load->view('pages/management', $donnees);
          }
          //si le joueur n'a pas le droit de renvoyer l'employé
          else
          {
            echo "Vous n'avez pas le droit de renvoyer cet employé<br>";
          }

        }

        /**
         * \brief   fonction permettant de remplacer l'employé renvoyé par un nouveau
         * \details la fonction supprime de la base des employés l'employé que l'on
         *          licencie et ajoute le candidat sélectionné à la liste des employés
         */
        public function remplacer()
        {
          //imports
          $this->load->library('Employe');
          $this->load->model('MEmploye');
          //on récupère l'employé renvoyé
          $renvoi = $this->input->post('renvoi');

          //on récupère les attribus de l'employé choisi
          $data = array('chef' => $this->input->post('chef'),
                        'competences' => $this->input->post('competences'),
                        'ambition' =>  $this->input->post('ambition'),
                        'moral' =>  $this->input->post('moral'),
                        'salaire' =>  $this->input->post('salaire'),
                        'sante' =>  $this->input->post('sante'),
                        'rang' =>  $this->input->post('rang'),
                        'nom' =>  $this->input->post('nom'),
                        'prenom' =>  $this->input->post('prenom'));

            //on sauvegarde l'employé sélectionné dans la base
            $nouveau = $this->MEmploye->create($data);
            $this->MEmploye->update_chef($renvoi, $nouveau->id(), $nouveau->rang() + 1);
            $this->MEmploye->renvoyer($renvoi);

            //on affiche un message
            echo "L'employé a bien été remplacé. Raffraichissez l'arbre pour voir les changements.";
        }

        /**
         * \brief   fonction affichant le panneau permettant d'améliorer un employé
         * \details le panneau ne peut améliorer que certains attribus de l'employé
         *          à savoir le salaire et les compétences de ce dernier
         * \param   id identifiant de l'employé que l'on souhaite améliorer
         */
        public function amelioration($id)
        {
          $this->load->library('Employe');
          $this->load->model('MEmploye');
          $emp = $this->MEmploye->get_by_id($id);
          if($emp->chef() == $this->MEmploye->get_user_empid())
          {
            $data['emp'] = $emp;
            $this->load->view('pages/ameliorer', $data);
          }
          else
          {
            echo "Vous n'avez pas le droit d'ameliorer le profil de cet employé<br>";
          }
        }

        /**
         * \brief   fonction récupérant les données envoyées par le joueur
         *          lors de l'amélioration d'un employé
         * \details c'est cette fonction qui se charge de faire les calculs nécessaires
         *          pour appliquer les changements sur l'employé
         */
        public function ameliorer($id){
          $this->load->library('Employe');
          $this->load->model('MEmploye');
          $emp = $this->MEmploye->get_by_id($id);

          $competence = $emp->competences();

          $comp = $this->input->POST('competence');
          $amb = $this->input->POST('ambition');
          $sal = $this->input->POST('salaire');

          if (!(is_null($sal))) {
            $ambitionIncrease = (5 / 100) * $sal;
            $amb = intval($amb + $ambitionIncrease);

            if ($emp->competences() + $comp >= 100) {
              $emp->set_competences(100);
              $comp = 0;
            }
            elseif ($emp->competences() + $comp <= 0) {
              $emp->set_competences(20);
              $comp = 0;
            }
            if($emp->ambition() + $amb >= 100) {
              $emp->set_ambition(100);
              $amb = 0;
            }
            elseif ($emp->ambition() + $amb <= 0) {
              $emp->set_ambition(20);
              $amb = 0;
            }
            if($emp->salaire() + $sal <= 1000) {
              $emp->set_salaire(1000);
              $sal = 0;
            }
          }

          $this->MEmploye->save($emp);
          $data = array($comp,$amb,$sal);
          $this->MEmploye->ameliorer($id,$data);
        }
}
