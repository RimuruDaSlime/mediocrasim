<?php
/**
 * \file      Arbre.php
 * \author    François
 * \version   1.0
 * \date      14 Décembre 2017
 * \brief     Gère l'arbre hierarchique
 *
 * \details   Cette classe ce charge de gérer les différents calculs relatifs
 *            à l'arbre hierarchique et sa création
 *
 */
class Arbre extends CI_Controller {

  /**
   * \bief    fonction permettant d'afficher la page de l'arbre hiérarchique
   * \details c'est cette fonction qui va faire appel aux différentes vues et
   *          leur fournir les données qu'il faut pour construire l'arbre
   */
    public function view()
    {
      $this->load->library('Employe');
      $this->load->model('MEmploye');
      $this->load->model('MUser');
      $data['user'] = $this->MEmploye->get_by_id($this->MEmploye->get_user_empid());
      $data['tour'] = $this->MUser->tours();
      $this->load->view('templates/header', $data);
      $data['json'] = $this->get();
      $this->load->view('pages/hierarchie', $data);
      $this->load->view('templates/footer');
    }

    /**
     * \brief   fonction permettant de construire les données permettant à la vue
     *          hierarchie de construire l'arbre hierarchique
     * \details cette fonction retourne un objet JSON avec les différentes
     *          valeurs des employés
     * \return  un objet json qui permettra au script coté client d'afficher l'arbre hierarchique
     */
    public function get()
    {
      $this->load->library('Employe');
      $this->load->model('MEmploye');

      $emps = $this->MEmploye->get_all();

      $data = array();

      foreach ($emps as $emp) {
        $values = array('key' => $emp->id(),
                        'name' => $emp->prenom().' '.$emp->nom(),
                        'title' => "employe");
        if ($emp->chef() != 0)
        {
          $values['parent'] = $emp->chef();
        }

        switch ($emp->rang()) {
          case 1:
            $values['title'] = 'PDG';
            break;
          case 2:
            $values['title'] = 'Directeur';
            break;
          case 3:
            $values['title'] = 'Ingénieur';
            break;
          case 4:
            $values['title'] = 'Technicien';
            break;
          default:
            $values['title'] = 'Ouvrier';
            break;
        }
        $data[] = $values;

      }
      $obj = new stdClass();
      $obj->class = "go.TreeModel";
      $obj->nodeDataArray = $data;
      return json_encode($obj);
    }

    /**
     * \brief   fonction permettant de créer un arbre
     * \details l'arbre hierarchique sera constitué d'employés générés aléaleatoirement
     *          à partir des valeurs hauteur et taille passés en post
     */
    public function create()
    {
      $this->load->library('Employe');
      $this->load->model('MEmploye');

      if(isset($_POST['hauteur']) && isset($_POST['taille']))
      {
        $this->db->empty_table('employes');
        $this->MEmploye->generer_arbre($_POST['hauteur'], $_POST['taille']);
        redirect('index.php/hierarchie');
      }
      else
      {
        echo "Erreur";
      }
    }
}
