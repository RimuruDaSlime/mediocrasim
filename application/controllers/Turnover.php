<?php
/**
 * \file      Turnover.php
 * \author    François
 * \version   1.0
 * \date      14 decembre 2017
 * \brief     Gère les changements lors de la fin d'un tour
 *
 * \details   Cette classe se charge de faire les changements et les calculs nécessaires
 *            pour que le moteur du jeu puisse tourner correctement et notamment gérer
 *            l'évolution de l'arbre hierarchique ainsi que le déroulement des
 *            évènements aléatoires à chaque fin de tour.
 */
class Turnover extends CI_Controller {

    //champs de la classe
    public $hauteur;
    public $random = false;
    public $lose = false;
    public $colleguesEmpPromu = array();

    /**
     * \brief   fais les calcul nécessaires pour la fin du tour
     * \details cette fonciton s'occupe de désigner les employés partant de l'entreprise
     *          et de les remplacer, c'est aussi elle qui va se charger de remplacer les employés
     *          laissant leur poste pour en prendre un nouveau, et de l'embauche d'éventuels nouveaux
     *          éléments dans l'entreprise
     * \param
     * \return
     */
    public function next_turn()
    {
      //import des différentes librairies
      $this->load->library('Employe');
      $this->load->model('MEmploye');
      $this->load->model('MDeparts');
      $this->load->model('MUser');
      $this->load->model('MChoix');

      //si l'utilisateur n'a pas encore renseigné quel employé il souhaitait promouvoir
      if($this->MChoix->est_vide()){
        $this->promouvoir();
      }
      else
      {
        //on vide la table des départs
        $this->MDeparts->vider();
        if($this->MUser->get_IA() == 1)
          $this->random = true;

        //on récupère la hauteur de l'arbre
        $this->hauteur = $this->MEmploye->get_hauteur();
        //on récupère tous les employés et cherchons les départs
        $employes = $this->MEmploye->get_all();
        $departs = $this->get_departs($employes);

        $user = $this->MEmploye->get_user_empid();
        $demande = array();
        //pour chaque employé quittant l'entreprise, on se charge de le remplacer
        foreach ($departs as $emp) {
          //ajout du départ à la liste des départs dans la base de données
          if($emp->id() == $user)
          {
            $this->lose = true;
          }
          else
          {
            $data = array('titre' => 'Départ',
                          'description' => 'Départ de '.$emp->prenom().' '.$emp->nom().' de rang '.$emp->rang());
            $this->MDeparts->ajouter($data);
            $emp = $this->MEmploye->get_by_id($emp->id());  //si jamais il y a eu des modifications sur l'employé avant
            $this->remplacer($emp, true);
          }
        }
        //on regarde si le joueur a perdu
        if ($this->lose || $this->MEmploye->get_by_id($this->MEmploye->get_user_empid())->moral() <= 0)
        {
          $this->db->empty_table('user');
          $this->db->empty_table('departs');
          $this->db->empty_table('employes');

          $this->load->view('pages/lose');
        }

        //une fois que toutes les modifcations sont faites sur les employés, on regarde si le joueur a gagné
        else if($this->check_win())
        {
          $this->db->empty_table('user');
          $this->db->empty_table('departs');
          $this->db->empty_table('employes');
          //affichage d'une page de victoire
          $this->load->view('pages/win');
        }
        //si le joueur n'a pas encore gagné, on continue le jeu en le redirigeant vers le site
        else
        {
          //ajout d'un tour au compteur
          $this->MUser->add_tour();
          $this->MChoix->vider();
          $this->evenementsAleatoires();
          redirect('/index.php/accueil');
        }
      }

    }
    /**
     * \brief     fonction permettant d'afficher le panneau pour promouvoir des employés
     *            et d'enregistrer le choix de l'utilisateur dans la base de données
     * \details   lors de chaque fin de tour, le joueur décide de quel employé de son
     *            équipe il souhaite promouvoir. C'est cette fonciton qui va lui afficher la
     *            vue qui va lui permettre de faire le choix, et qui va stocker l'ID de
     *            l'employé ainsi promu dans la table choix de la base de données.
     * \param
     * \return
     */
    public function promouvoir()
    {
      $this->load->library('Employe');
      $this->load->model('MEmploye');
      switch ($_SERVER['REQUEST_METHOD'])
      {
        case 'GET' :
          $emps = $this->MEmploye->get_equipe($this->MEmploye->get_user_empid());
          $data = array("employes" => $emps);
          $this->load->view('pages/promouvoir', $data);
          break;
        case 'POST' :
          $this->load->model("MChoix");
          $this->MChoix->create(array("id" => $this->input->post('emp')));
          $emps = $this->MEmploye->get_equipe($this->MEmploye->get_user_empid());
          foreach($emps as $e)
          {
            if($e->id() != $this->input->post('emp'))
            {
              $this->colleguesEmpPromu[] = $e->id();
            }
          }
          $this->next_turn();
        break;

      }

    }

    /**
     * \brief   fonction permettant de regarder si le joueur a gagné
     * \details la fonction regarde le rang du joueur : si ce dernier vaut 1
     *          alors le joueur est devenu PDG, sinon, il a encore des échelons à monter.
     * \param
     * \return  valeur de vérité de l'assertion "Le joueur est devenu le PDG de l'entreprise"
     */
    public function check_win()
    {
      return $this->MEmploye->get_by_id($this->MEmploye->get_user_empid()) ->rang() == 1;
    }

    /**
     * \brief             fonction désignant les départs au sein de l'entreprise
     * \details           un employé peut partir pour deux raisons : ce dernier n'est pas
     *                    assez rentable et est donc licencié, ou bien c'est un départ volontaire
     *                    (simulé par le tirage d'un chiffre aléatoire)
     * \param   $employes liste des employés de l'entreprise
     * \return  $departs  liste des employés partant de l'entreprise
     */
    public function get_departs($employes)
    {

      $departs = array(); //tableau dans lequel seront stockées les employés partant de l'entreprise
      foreach ($employes as $emp)
      {
          //on sélectiionne les employés non rentables ou ceux qui désirent partir de l'entreprise
          if(!($emp->rentabilite() >= 0))
          {
            //ajout de l'employé à la liste des départs
            $departs[] = $emp;
          }
          else if(($emp->id() != $this->MEmploye->get_user_empid()) && rand(1,100) > 90)
          {
            //ajout de l'employé à la liste des départs
            $departs[] = $emp;
          }
      }
      //on retourne la liste des départs
      return $departs;
    }



    /**
     * \brief         fonction retournant l'identifiant de l'employé le plus rentable d'un rang donné
     * \details       la rentabilité de l'employé est calculée grâce à la méthode rentabilité de la classe employé
     *                il s'agit uniquement d'une recherche de maximum dans une liste d'employés
     * \param   $rang rang auquel on veut connaître l'employé le plus rentables
     * \return  $id   identifiant de l'employé le plus rantable du rang
     */
    public function get_meilleur_rang($rang)
    {

      $max = -1000000;
      $maxemp = null;

      //on récupère tous les employés du rang passé en paramètres
      $emps = $this->MEmploye->get_by_rang($rang);
      $emps = $this->listeChoix($emps);

      //on boucle sur la liste d'employés pour trouver le plus rentable de la liste
      foreach($emps as $emp)
      {
          $rent = $emp->rentabilite();
          //si l'employé est le plus rentable jusqu'à présent
          if($rent > $max)
          {
            $max = $rent;   //on met à jour la rentabilité maximale que l'on a pour l'instant
            $maxemp = $emp; //on stocke l'identifiant de l'employé
          }
      }
      //on renvoie l'identifiant de l'employé le plus rentable
      return $maxemp->id();
    }

    /**
     * \brief           fonction retournant la liste des employés qui sont susceptibles de changer de poste
     * \details         en fonction de l'employé que le joueur a décidé de promouvoir, la liste des
     *                  employés susceptibles d'être promus n'est pas la même. Cette fonciton se charge donc
     *                  d'enlever les employés de l'équipe du joueur qui n'ont pas été promus
     *                  par ce dernier d'une liste d'employés
     * \param   $liste  liste des employés dpnt on veut enlever les employés qui n'ont pas été promus
     * \return  $res    la liste des employés avec les changements effectués
     */
    public function listeChoix($liste)
    {
      $res = array();
      foreach ($liste as $emp) {
        if(!in_array($emp->id(), $this->colleguesEmpPromu))
        {
          $res[] = $emp;
        }
      }

      return $res;
    }

    /**
     * \brief         désigne l'employé qui remplacera un poste dans l'IA random
     * \details       on sélectionne tout simplement un employé dans la liste des employés
     *                du rang passé en argument à la fonction
     * \param   $rang rang auquel on veut tirer un employé aléatoirement
     * \return  $emp  l'employé (l'objet employé, et non l'identifiant comme c'était le cas
     *                avec la fonction get_meilleur_rang) tiré au hasard
     */
    public function get_random($rang)
    {
      //on récupère les employés du rang passé en argument
      $emps = $this->MEmploye->get_by_rang($rang);
      //on tire un entier entre 0 et le nombre d'employés de la liste - 1
      $index = rand(0, count($emps) -1);
      //on sélectionne l'employé de l'index tiré au hasard dans la liste
      $emp = $emps[$index];
      //on retourne l'employé
      return $emp;
    }

    /**
     * \brief           fontion remplacer servant à remplacer un employé quittant son poste
     * \details         cette fonction gère les changements de poste au sein de l'arbre, et
     *                  se charge d'embaucher de nouveaux employés si besoin. C'est aussi elle
     *                  qui va se charger de demander la suppression de l'entrée de l'employé
     *                  dans la base de données si jamais ce dernier part définitiviement de
     *                  l'entreprise.
     * \param   emp     objet employe de l'employé que l'on veut remplacer
     * \param   renvoi  boolean qui indique si l'employé part définitivement de l'entreprise
     *                  ou si au contraire ce dernier change juste de poste.
     * \param   chef    argument servant à indiquer le chef d'un candidat que l'on embaucherait
     *                  dans l'entreprise
     * \return
     */
    public function remplacer($emp, $renvoi=false, $chef=null)
    {
      $this->load->model('MEmploye');
      $this->load->library('Employe');
      //on regarde si l'employé peut être remplacé par quelqu'un dans l'entreprise
      if($emp->rang() < $this->hauteur)
      {
        //en fonction de l'IA sélectionnée, on choisit le remplacant différemment
        if($this->random)
          $remplacant = $this->get_random($emp->rang() +1); //si l'IA random est l'IA actuelle
        else
          $remplacant = $this->MEmploye->get_by_id($this->get_meilleur_rang($emp->rang()+ 1)); //si l'IA normale est l'IA actuelle

        //on note la promotion de l'employé sélectionné dans la base de donnée
        $data = array('titre' => 'Promotion',
                      'description' => 'Promotion de '.$remplacant->prenom().' '.$remplacant->nom().' pour remplacer '.$emp->prenom().' '.$emp->nom());
        $this->MDeparts->ajouter($data);

        //si le remplacant remplace son chef, alors si l'on embauche un nouvel employé pour le remplacer, son chef sera le remplacant
        if($emp->id() == $remplacant->chef())
          $chef = $remplacant->id();
        //sinon, il s'agit bien de l'ancien chef du remplacant
        else
          $chef = $remplacant->chef();

        //on remplace le remplacant
        $this->remplacer($remplacant, false, $chef);
        $rang = $remplacant->rang();

        //on augmente le remplacant
        $remplacant->augmenter($emp->id());
        $remplacant->set_chef($emp->chef());
        $this->MEmploye->save($remplacant);

        //on met à jour le chef
        $this->MEmploye->update_chef($emp->id(), $remplacant->id(), $rang);
      }
      //création d'un nouvel employé
      else
      {
        //cas où l'employé juste avant est parti de l'entreprise
        if($renvoi)
          $chef = $emp->chef(); //on hérite de son chef

        //on embauche un nouvel employé pour remplacer le poste vacant
        $remplacant = $this->MEmploye->emp_aleatoire($chef, $this->hauteur, 1000);
        //sauvegarde de cet employé dans la base de données des employés
        $this->MEmploye->save($remplacant);
        //on note le changement dans les rapports de fin de tour
        $data = array('titre' => 'Embauche',
                      'description' => 'Embauche de '.$remplacant->prenom().' '.$remplacant->nom().' pour remplacer '.$emp->prenom().' '.$emp->nom());
        $this->MDeparts->ajouter($data);
      }

      //on renvoie l'employé s'il s'agit d'un renvoi
      if($renvoi)
        $this->MEmploye->renvoyer($emp->id());
    }

    /**
     * \brief   fonction gérant les évènements aléatoires qui sont susceptibles de
     *          survenir à la fin de chaque tour
     * \details on tire au hasard un évènement aléatoire dans une liste prédéfinié
     *          et on fait les calculs qui lui sont rattachés
     * \return
     */
    public function evenementsAleatoires()
    {
      //import de la bibliothèque MEvenements
      $this->load->model("MEvenements");
      //on vide la base qui contient encore les évènements aléatoires du tour dernier
      $this->MEvenements->vider();
      //on tire au hasard un entier entre 0 et 15 (inclus)
      $index = rand(0,15);
      //on tire au hasard un taux qui déterminera la sévérité de l'évènement aléatoire sélectionné
      $taux = rand(1,10);

      //en fonction de l'index tiré au hasard
      switch ($index) {
        case 0:
          $evenement = "Crash Boursier";
          $consequence = "Le moral de tous les employés baisse de ".$taux."%";
          $this->MEmploye->modifierMoralTousLesEmployes($taux * -1);
          break;

        case 1:
          $evenement = "Scandale";
          $consequence = "Les comptes bancaires au Panama du PDG ne favorisent pas l'opinion publique. Tout le monde perd ".$taux."% d'ambition";
          $this->MEmploye->modifierAmbitionTousLesEmployes($taux * -1);
          break;

        case 2:
          $evenement = "Très bon semestre";
          $consequence = "Tous les employés voient leur salaire augmenter de 10%";
          $this->MEmploye->modifierSalaireTousLesEmployes(0.10);
          break;

        case 3:
          $evenement = "Attaque en justice";
          $consequence = "Oups, l'entreprise a oublié de déclarer certains revenus.. Le moral de tous les employés ainsi que leur santé (stress) baissent de ".$taux."%";
          $this->MEmploye->modifierMoralTousLesEmployes($taux * -1);
          $this->MEmploye->modifierSanteTousLesEmployes($taux * -1);
          break;

        case 4:
          $evenement = "Votre chef s'est fait larguer";
          $consequence = "Par compassion votre moral baisse de ".$taux."%";
          $emp = $this->MEmploye->get_by_id($this->MEmploye->get_user_empid());
          $emp->set_moral($emp->moral() - $taux);
          $this->MEmploye->save($emp);
          break;

        case 5:
          $evenement = "Vous récompensez vos employés";
          $consequence = "Vous augmentez leur salaire de 5%";
          $emps = $this->MEmploye->get_equipe($this->MEmploye->get_user_empid());
          foreach ($emps as $emp) {
            $emp->set_salaire($emp->salaire() + ($emp->salaire() * 0.05));
            $this->MEmploye->save($emp);
          }
          break;

        case 6:
          $evenement = "La nouvelle campagne de pub est très effective";
          $consequence = "L'argent coule à flot, les salaires de tous les employés sont augmentés de 15%";
          $this->MEmploye->modifierSalaireTousLesEmployes(0.15);
          break;

        case 7:
          $evenement = "Projets non finis";
          $consequence = "On vous avait dit de vous y prendre à l'avance.. Votre stress augmente (baisse de santé) de ".$taux."%";
          $emp = $this->MEmploye->get_by_id($this->MEmploye->get_user_empid());
          $emp->set_sante($emp->moral() - $taux);
          $this->MEmploye->save($emp);
          break;

        case 8:
          $evenement = "Le chauffage de l'entreprise ne fonctionne plus";
          $consequence = "Vous prenez froid et votre santé baisse de ".$taux."%";
          $emp = $this->MEmploye->get_by_id($this->MEmploye->get_user_empid());
          $emp->set_sante($emp->moral() - $taux);
          $this->MEmploye->save($emp);
          break;

        case 9:
          $evenement = "L'entreprise adopte de nouvelles technologies";
          $consequence = "Les compétences des employés baissent de ".$taux."%";
          $this->MEmploye->modifierCompetencesTousLesEmployes($taux * -1);
          break;

        default:
          $evenement = "Aucun évènement ce tour ci";
          $consequence = "Vous pouvez travailler tranquillement";
          break;
      }
      //on note l'évenement aléatoire dans la base des évènements aléatoires
      $data = array('evenement' => $evenement,
                    'consequence' => $consequence);
      $this->MEvenements->ajouter($data);
    }

}
