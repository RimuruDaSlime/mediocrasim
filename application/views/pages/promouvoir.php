<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
<div class="card-panel teal">
  <span class="white-text">
    Si jamais un poste du même niveau se libère, qui voulez-vous promouvoir ?
  </span>
</div>
<div class="row">
  <?php foreach ($employes as $emp){?>

    <div class="col s12 m6">
        <div class="card">
          <div class="card-content cyan white-text">
            <span class="card-title"><?= $emp->prenom().' '.$emp->nom()?></span>
            <table>
              <thead>
                <tr>
                    <th>Critère</th>
                    <th>Niveau</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>Compétences</td>
                  <td>
                    <div class="progress">
                        <div class="determinate" style="width: <?= $emp->competences()?>%"></div>
                    </div>
                    <?= $emp->competences()?>%
                  </td>
                </tr>
                <tr>
                  <td>Ambition</td>
                  <td>
                    <div class="progress">
                        <div class="determinate" style="width: <?= $emp->ambition()?>%"></div>
                    </div>
                    <?= $emp->ambition()?>%
                  </td>
                </tr>
                <tr>
                  <td>Productivité</td>
                  <td>
                    <?= $emp->productivite()?>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="card-action" id="<?= $emp->id(); ?>">
            <form class="" action="<?= site_url('/index.php/turnover/promouvoir')?>" method="post" type="hidden">
              <input class="btn cyan" type="hidden" name="emp" value="<?= $emp->id() ?>">
              <input class="btn cyan" type="submit" name="" value="promouvoir">
            </form>
          </div>
        </div>
      </div>
        <?php } ?>
    </div>
    </div>
