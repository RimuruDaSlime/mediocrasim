<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
<div class="col s6 m6">
  <div class="card">
    <div class="card-content cyan white-text">
      <span class="card-title"><?= $emp->prenom().' '.$emp->nom()?></span>
      <form method="POST" action="<?= base_url('index.php/pages/ameliorer').'/'.$emp->id()?>">
      <table>
        <thead>
          <tr>
              <th>Critère</th>
              <th>Niveau</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Compétence</td>
            <td>
              <div class="progress">
                  <div class="determinate" style="width: <?= $emp->competences()?>%"></div>
              </div>
              <?= $emp->competences()?>%
            </td>
            <td><input type="text-box" name="competence"></td>
          </tr>
          <tr>
            <td>Salaire</td>
            <td>
              <?= $emp->salaire()?>€
            </td>
            <td><input type="text-box" name="salaire"></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="card-action">
      <input type="submit" value="Ameliorer" class="btn">
    </div>
    </form>
    <script type="text/javascript">
      window.onunload = function() {
        window.opener.location.reload();
        window.close();
      }
    </script>
  </div>
</div>
