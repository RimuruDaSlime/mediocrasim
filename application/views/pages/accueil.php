<div id="card-stats">
              <div class="row">
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">work</i>
                        <p>Productivité</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h5 class="mb-0"><?=number_format($user->productivite())?></h5>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-red-pink gradient-shadow min-height-100 white-text">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">perm_identity</i>
                        <p>Votre chef</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h5 class="mb-0"><?= $chef->prenom().' '.$chef->nom()?></h5>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">timeline</i>
                        <p>Rentabilité</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h5 class="mb-0"><?= number_format($user->rentabilite())?></h5>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col s12 m6 l3">
                  <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text">
                    <div class="padding-4">
                      <div class="col s7 m7">
                        <i class="material-icons background-round mt-5">attach_money</i>
                        <p>Salaire</p>
                      </div>
                      <div class="col s5 m5 right-align">
                        <h5 class="mb-0"><?= $user->salaire()?>€</h5>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col s12 m6 l3">
                <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">healing</i>
                      <p>Santé</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?=number_format($user->sante())?>%</h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col s12 m6 l3">
                <div class="card gradient-shadow gradient-45deg-amber-amber min-height-100 white-text">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">mood</i>
                      <p>Moral</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?=number_format($user->moral())?>%</h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col s12 m6 l3">
                <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">directions_run</i>
                      <p>Ambition</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?=number_format($user->ambition())?>%</h5>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col s12 m6 l3">
                <div class="card gradient-45deg-red-pink gradient-shadow min-height-100 white-text">
                  <div class="padding-4">
                    <div class="col s7 m7">
                      <i class="material-icons background-round mt-5">star</i>
                      <p>Compétences</p>
                    </div>
                    <div class="col s5 m5 right-align">
                      <h5 class="mb-0"><?=number_format($user->competences())?>%</h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col s12 m12 l6">
                              <ul id="issues-collection" class="collection z-depth-1">
                                <li class="collection-item avatar">
                                  <i class="material-icons cyan accent-2 circle">directions_run</i>
                                  <h6 class="collection-header m-0">Rapport de fin de tour</h6>
                                </li>
                                <?php
                                foreach($departs as $d) { ?>
                                <li class="collection-item">
                                  <div class="row">
                                    <div class="col s7">
                                      <p class="collections-title">
                                        <?= $d['titre']?>
                                      </p>
                                      <p class="collections-content"></p>
                                    </div>
                                    <div class="col s5">
                                      <p><?= $d['description']?></p>
                                    </div>
                                  </div>
                                </li>
                              <?php }?>
                              </ul>
                            </div>
                            <div class="col s12 m12 l6">
                              <ul id="projects-collection" class="collection z-depth-1">
                                <li class="collection-item avatar">
                                  <i class="material-icons red circle">report_problem</i>
                                    <h6 class="collection-header m-0">Évenements aléatoires</h6>
                                      <p>Au tour numéro <?= $tour ?></p>
                                </li>
                                <?php foreach ($evenements as $ev) { ?>
                                <li class="collection-item">
                                  <div class="row">
                                    <div class="col s6">
                                      <p class="collections-title"><?= $ev['evenement']?></p>
                                    </div>
                                    <div class="col s5">
                                    <p><?= $ev['consequence']?></p>
                                    </div>
                                  </div>
                                </li>
                              <?php }?>
                              </ul>
              </div>
            </div>
