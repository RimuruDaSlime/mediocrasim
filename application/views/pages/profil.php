<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
<div class="col s6 m6">
  <div class="card">
    <div class="card-content cyan white-text">
      <?php
          $id=$emp->id();

                  $idString = (string)$id; // type casting int to string

                  $lastDigit= $idString[strlen($idString)-1];
                  if ($id==$user)
                  {
                    $lastDigit=67;
                  }

          echo "<img src=".base_url()."/assets/img/HS".$lastDigit.".svg width='100' height='75'>";
      ?>
      <span class="card-title"><?= $emp->prenom().' '.$emp->nom()?></span>
      <table>
        <thead>
          <tr>
              <th>Critère</th>
              <th>Niveau</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Compétence</td>
            <td>
              <div class="progress">
                  <div class="determinate" style="width: <?= $emp->competences()?>%"></div>
              </div>
              <?= $emp->competences()?>%
            </td>
          </tr>
          <tr>
            <td>Ambition</td>
            <td>
              <div class="progress">
                  <div class="determinate" style="width: <?= $emp->ambition()?>%"></div>
              </div>
              <?= $emp->ambition()?>%
            </td>
          </tr>
          <tr>
            <td>Productivité</td>
            <td>
              <?= number_format ( $emp->productivite(), 2)?>
            </td>
          </tr>
          <tr>
            <td>Salaire</td>
            <td>
              <?= $emp->salaire()?>€
            </td>
          </tr>
          <tr>
            <td>Taux de présence</td>
            <td>
              <?= number_format($emp->presence())?>%
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
