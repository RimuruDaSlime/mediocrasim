<!DOCTYPE html>
<html>
  <head>
    <title>Mediocrasim</title>
    <meta charset="utf-8">
    <!--Import de Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import de materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <!--Import de la bibliothèque de création d'arbres hierarchiques-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gojs/1.7.26/go-debug.js"></script>
    <!--Optimisation du site pour mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>

  <body>

    <main>
      <div class="container">
      <form class="" action="<?= site_url('index.php/pages/setup/')?>" method="post">
        <h3>Paramétrage de votre employé</h3>
        <label for="prenom">Prénom</label>
        <input type="text" name="prenom" value="">
        <label for="nom">Nom</label>
        <input type="text" name="nom" value="">
        <label for="">Compétence de votre employé:</label>
        <p class="range-field">
          <input type="range" name="competence" id="competence" min="10" max="100" value="75"/>
        </p>
        <label for="">Moral</label>
        <p class="range-field">
          <input type="range" name="moral" id="moral" min="10" max="100" value="75"/>
        </p>
        <label for="">Ambition</label>
        <p class="range-field">
          <input type="range" name="ambition" id="ambition" min="10" max="100" value="100"/>
        </p>
        <h3>Paramétrage de l'entreprise</h3>

        <input type="hidden" name="hauteur" id="hauteur" value="3">
        <input type="hidden" name="taille" id="taille" value="2">
            <p>
              <input class="with-gap" name="group1" type="radio" id="test1"
              onclick="hauteur.value=3;taille.value=2;" checked />
              <label for="test1">Petite entreprise</label>
            </p>
            <p>
              <input class="with-gap" name="group1" type="radio" id="test2"
              onclick="hauteur.value=4;taille.value=2;"/>
              <label for="test2">Moyenne entreprise</label>
            </p>
            <p>
              <input class="with-gap" name="group1" type="radio" id="test3"
              onclick="hauteur.value=4;taille.value=3;"/>
              <label for="test3">Grande entreprise</label>
            </p>
            <p>
              <input class="with-gap" name="group1" type="radio" id="test4"
              onclick="hauteur.value=5;taille.value=3;"/>
              <label for="test4">Multinationale</label>
            </p>

        <br>

        <h3>Choix de l'IA</h3>

        <p>
          <input class="with-gap" name="IA" value="random" type="radio" id="random" checked/>
          <label for="random">IA Random</label>
        </p>
        <p>
          <input class="with-gap" name="IA" value="normale" type="radio" id="normale" />
          <label for="normale">IA normale</label>
        </p>

        <br>
        <input class="btn" type="submit" name="" value="Créer la partie">

      </form>
    </div>
    </main>
