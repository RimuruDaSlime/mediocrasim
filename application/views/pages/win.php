<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <title>Game Over</title>
  </head>
  <body>
    <div class="row">
      <div class="container center-align">
        <h1>Félicitations</h1>
        <h3>Vous êtes devenu le PDG de cette entreprise!!</h3>
        <a href="<?= site_url('index.php/accueil') ?>" class="waves-effect waves-light btn">Rejouer</a>
      </div>
    </div>
  </body>
</html>
