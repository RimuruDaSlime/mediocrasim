<div class="hire">
  <div class="row">
    <?php foreach ($employes as $emp) {?>

      <div class="col s6 m6">
        <div class="card">
          <div class="card-content cyan white-text">

          <?php
            echo "<img src=".base_url()."/assets/img/HS".substr($emp->id(), -1).".svg width='100' height='75'>";  
          ?>
          <span class="card-title"><?= $emp->prenom().' '.$emp->nom()?></span>
            <table>
              <thead>
                <tr>
                    <th>Critère</th>
                    <th>Niveau</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>Compétences</td>
                  <td>
                    <div class="progress">
                        <div class="determinate" style="width: <?= $emp->competences()?>%"></div>
                    </div>
                    <?= $emp->competences()?>%
                  </td>
                </tr>
                <tr>
                  <td>Ambition</td>
                  <td>
                    <div class="progress">
                        <div class="determinate" style="width: <?= $emp->ambition()?>%"></div>
                    </div>
                    <?= $emp->ambition()?>%
                  </td>
                </tr>
                <tr>
                  <td>Productivité</td>
                  <td>
                    <?= number_format ( $emp->productivite(), 2)?>
                  </td>
                </tr>
                <tr>
                  <td>Salaire</td>
                  <td>
                    <?= $emp->salaire()?>€
                  </td>
                </tr>
                <tr>
                  <td>Taux de présence</td>
                  <td>
                    <?= number_format($emp->presence())?>%
                  </td>
                </tr>
                <tr>
                  <td>Santé</td>
                  <td>
                    <?= number_format($emp->sante())?>%
                  </td>
                </tr>
                <tr>
                  <td>Moral</td>
                  <td>
                    <?= number_format($emp->moral())?>%
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="card-action">
            <a href="#" onclick="window.open('<?= site_url('index.php/amelioration')?>/<?= $emp->id()?>', '', 'width=800,height=500');
              return false;">Améliorer</a>
            <a href="#" onclick="window.open('<?= site_url('index.php/renvoi')?>/<?= $emp->id()?>', '', 'width=800,height=500');
              return false;">Renvoyer</a>
          </div>
        </div>
      </div>
        <?php } ?>
    </div>
</div>

</main>
