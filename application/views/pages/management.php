<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
<div class="card-panel teal">
  <span class="white-text">Avant de renvoyer l'employé, veuillez en choisir son remplaçant !
  </span>
</div>
<div class="row">
  <?php foreach ($employes as $emp){?>

    <div class="col s12 m6">
        <div class="card">
          <div class="card-content cyan white-text">
            <span class="card-title"><?= $emp->prenom().' '.$emp->nom()?></span>
            <table>
              <thead>
                <tr>
                    <th>Critère</th>
                    <th>Niveau</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>Compétences</td>
                  <td>
                    <div class="progress">
                        <div class="determinate" style="width: <?= $emp->competences()?>%"></div>
                    </div>
                    <?= $emp->competences()?>%
                  </td>
                </tr>
                <tr>
                  <td>Ambition</td>
                  <td>
                    <div class="progress">
                        <div class="determinate" style="width: <?= $emp->ambition()?>%"></div>
                    </div>
                    <?= $emp->ambition()?>%
                  </td>
                </tr>
                <tr>
                  <td>Productivité</td>
                  <td>
                    <?= $emp->productivite()?>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="card-action" id="<?= $emp->id(); ?>">
            <form class="" action="<?= site_url('/index.php/pages/remplacer')?>" method="post" type="hidden">
              <input type="hidden" name="prenom" readonly value="<?= $emp->prenom() ?>">
              <input type="hidden" name="nom" readonly value="<?= $emp->nom()?>">
              <input type="hidden" name="competences" readonly value="<?= $emp->competences()?>">
              <input type="hidden" name="moral" readonly value="<?= $emp->moral() ?>">
              <input type="hidden" name="ambition" readonly value="<?= $emp->ambition()?>">
              <input type="hidden" name="productivite" readonly value="<?= $emp->productivite()?>">
              <input type="hidden" name="chef" readonly value="<?= $emp->chef(); ?>">
              <input type="hidden" name="salaire" readonly value="<?= $emp->salaire() ?>">
              <input type="hidden" name="rang" readonly value="<?= $emp->rang() ?>">
              <input type="hidden" name="renvoi" readonly value="<?=$renvoi?>">
              <input type="hidden" name="sante" readonly value="<?= $emp->sante()?>">
              <input type="submit" name="" value="Embaucher">
            </form>
          </div>
        </div>
      </div>
        <?php } ?>
    </div>
    </div>

    <script type="text/javascript">
    window.onunload = function() {
      window.opener.location.reload();
      window.close();
    }
    </script>
