<!DOCTYPE html>
<html>
  <head>
    <title>Mediocrasim</title>
    <meta charset="utf-8">
    <!--Import de Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import de materialize.css-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <!--Import du css spécifique au projet-->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/style.css">
    <!--<link rel="stylesheet" href="<?= base_url()?>assets/css/jeu.css">-->

    <!--Import de la bibliothèque de création d'arbres hierarchiques-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gojs/1.7.26/go-debug.js"></script>
    <!--Optimisation du site pour mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>

  <body>
    <header>
      <ul class="side-nav fixed" id="slide-out"> <!--Début de la barre de navigation-->
        <li>
          <div class="user-view">
            <div class="background">
              <img src="http://demo.geekslabs.com/materialize/v3.1/images/user-bg.jpg">
            </div>
            <img class='circle' src='<?= base_url()."/assets/img/HS1.svg" ?>' width='100' height='75'>
            <span class="white-text name"><?=$user->prenom().' '.$user->nom()?></span>
            <span class="white-text email">Rang <?=$user->rang()?> - Tour <?= $tour ?></span>
          </div>
        </li>
        <li><a href="<?= site_url('index.php/accueil')?>" class="waves-effect"><i class="material-icons">home</i>Accueil</a></li>
        <li><a href="<?= site_url('index.php/hierarchie')?>" class="waves-effect"><i class="material-icons">call_split</i>Hiérarchie</a></li>
        <li><a href="<?= site_url('index.php/equipe')?>" class="waves-effect"><i class="material-icons">people</i>Mon équipe</a></li>
        <li><a href="<?= site_url('index.php/pages/demission')?>" class="waves-effect waves-light btn red">Démissionner</a></li>
        <li><a href="<?= site_url('index.php/turnover/next_turn')?>" class="waves-effect waves-light btn">Fin du tour</a></li>

    </ul>
    <ul class="side-nav" id="mobile-demo">
      <li>
        <div class="user-view">
          <div class="background">
            <img src="http://demo.geekslabs.com/materialize/v3.1/images/user-bg.jpg">
          </div>
          <img class="circle" src="https://scontent-cdg2-1.xx.fbcdn.net/v/t1.0-9/14469603_1350491618308142_4965513064071893801_n.jpg?oh=2a604fd38cf6bd1771c54acf961ff730&oe=5A483026">
          <span class="white-text name"><?=$user->prenom().' '.$user->nom()?></span>
          <span class="white-text email">Rang <?=$user->rang()?> - Tour <?= $tour ?></span>
        </div>
      </li>
      <li><a href="<?= site_url('index.php/accueil')?>" class="waves-effect"><i class="material-icons">home</i>Accueil</a></li>
      <li><a href="<?= site_url('index.php/hierarchie')?>" class="waves-effect"><i class="material-icons">call_split</i>Hiérarchie</a></li>
      <li><a href="<?= site_url('index.php/equipe')?>" class="waves-effect"><i class="material-icons">people</i>Mon équipe</a></li>
      <li><a href="<?= site_url('index.php/turnover/next_turn')?>" class="waves-effect waves-light btn">Fin du tour</a></li>
    </ul><!--Fin de la barre de navigation-->
  </header>


<main>
