# Mediocrasim

Dans mediocrasim, vous jouez le rôle d'un manager ambitieux voulant à tout prix devenir le PDG de l'entreprise pour laquelle il travaille.
Pour cela, vous devrez apprendre à gérer efficacement une équipe afin d'avoir les meilleures statistiques possibles, et pouvoir être promu, jusqu'à devenir le grand patron.

## Pour commencer

Le jeu se joue avec un navigateur web (idéalement Firefox ou Chrome). Vous devrez configurer la partie avant de commencer à jouer, en choisissant notamment les attributs de votre personnage, le niveau de difficulté et la taille de l'entreprise dans laquelle vous travaillerez.
Il a été entièrement codé en HTML/CSS, JavaScript et PHP.
Notons que le jeu ne peut faire jouer qu'un seul joueur à la fois (nous n'avons pas eu le temps d'implémenter une gestion d'utilisateurs multiples), si une partie est déjà commencée lorsque vous arrivez sur l'écran du jeu (si vous avez déjà accès au tableau de bord, à l'arbre hierarchique etc), alors il vous suffit de cliquer sur le bouton rouge "Démissioner" pour recommencer une partie avec vos réglages.

### Conditions préalables

Pour y jouer : un navigateur web avec JavaScript activé. Seuls Firefox et Chrome ont été testés.
Pour installer le moteur du jeu chez vous : un serveur web faisant tourner PHP, ainsi qu'une base de données MySql.

### Installation

Pour installer le jeu en Local, tapez sur un terminal : git@gitlab.com:RimuruDaSlime/mediocrasim.git
Un certain nombre de paramètres doivent être changés :

- Créer les bases de données
- Connectez-vous et créez une nouvelle base de données, au nom de "mediocrasim"
- Cliquez sur cette nouvelle table, allez dans l'onglet importer et importez le fichier "init_db.sql" qui se trouve dans le dossier "assets" en appuyant sur "Choisir le fichier", puis exécutez. Vous pouvez fermer cette page et retourner dans le dossier initial créé "T432_GOJ17_T3_A"
- Allez dans "application" et enfin dans "config"
- Ouvrez le fichier "config.php"
- Modifiez la ligne 27 : $config['base_url']='', remplissez les côtes avec le lien de votre seveur web
- Enregistrer et fermer le fichier
- Ouvrez le fichier "database.php"
- modifiez les lignes 79 (username) et 80 (password) avec les identifiants que vous avez créer pour phpMyAdmin
- enregistrez et fermer
- vous pouvez désormais jouer en local en allant sur le lien de votre serveur web

## Déroulement d'une partie
- Le joueur commence par configurer sa partie grâce au panneau de configuration. Ici, il choisit le nom ainsi que les caractéristiques de son employé, mais aussi lé type d'IA et d'entreprise avec lesquels il souhaite jouer.
- Il va ensuite être redirigé sur l'écran d'accueil du jeu, où il pourra visionner ses stats, les différents changements dans la hierarchie, et évènements aléatoires.
- L'onglet Hiérarchie permet de visionner l'arbre hiérarchique, et si l'utilisateur fait un clic droit sur un poste, différentes actions vont être proposées à lui : renvoyer l'employé, visionner son profil ou encore améliorer l'employé.
- Enfin l'onglet Mon équipe permet de visionner l'équipe gérée par le joueur, et d'effectuer les mêmes actions que sur l'arbre.
- Lorsque le joueur a fait les changements qu'il souhaitait, ce dernier doit cliquer sur le bouton "Fin du tour" afin de passer au tour suivant.
- Une fenêtre va s'ouvrir, et l'utilisateur va devoir choisir l'employé qu'il souhaite promouvoir si un poste se libère au même niveau que celui du joueur
- Finalement, le tour passe, et les mises à jour sur l'arbre sont effectuées, et les différents évènement aléatoires et changements hiérarchiques seront expliqués sur l'écran d'accueil.
- L'utilisateur gagne la partie lorsqu'il devient PDG et la perd s'il se fait licencier ou si son moral atteint 0. Il peut mettre fin à la partie à tout moment en cliquant sur le bouton "démissioner".

## Captures d'écran
![page d'accueil](https://gitlab.com/RimuruDaSlime/mediocrasim/blob/master/Mediocrasim.PNG)

## Documentation
La documentation du projet est disponible [ici](http://iin-etu.iutrs.unistra.fr/~goudineau/Mediocrasim/Documentation/Mediocrasim/html/hierarchy.html)

## Construit avec

* [CodeIgniter](https://codeigniter.com) - Le framework web utilisé
* [Materialize CSS](http://materializecss.com) - Bibliothèque CSS
* [GoJs](https://gojs.net/latest/index.html) - Librairie JavaScript utilisée pour l'affichage de l'arbe


## Auteurs

* **François Goudineau** - [Francois Goudineau](https://git.unistra.fr/goudineau)
* **Ismaël El Kasbaoui** - [Ismaël El Kasbaoui](https://git.unistra.fr/elkasbaoui)
* **Levi Terante** - [Levi Terante](https://git.unistra.fr/terante)


